/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tlistdelone.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:50:28 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/30 17:55:04 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	free_redirs(t_slist **redirs)
{
	t_slist	*iter;
	t_slist	*tmp;

	iter = *redirs;
	while (iter)
	{
		tmp = iter->next;
		ft_strdel((char**)&((t_redir*)iter->data)->file);
		ft_strdel(&((t_redir*)iter->data)->heredoc);
		free(iter->data);
		free(iter);
		iter = tmp;
	}
	*redirs = NULL;
}

void	ft_tlistdelone(t_tlist **list)
{
	t_tlist	*prev;
	t_tlist	*next;
	int		i;

	prev = (*list)->prev;
	next = (*list)->next;
	i = -1;
	if ((*list)->t->args != NULL)
	{
		while ((*list)->t->args[++i] != NULL)
			free((*list)->t->args[i]);
	}
	free((*list)->t->args);
	free((*list)->t->value);
	free_redirs(&(*list)->t->redirs);
	free((*list)->t);
	free(*list);
	*list = next;
	if (*list != NULL)
		(*list)->prev = prev;
	if (prev != NULL)
		prev->next = *list;
}

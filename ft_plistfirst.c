/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistfirst.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:44:36 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/18 17:21:16 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_plist	*ft_plistfirst(t_plist *list)
{
	while (list->prev != NULL)
		list = list->prev;
	return (list);
}

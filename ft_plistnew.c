/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistnew.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:34:51 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/18 17:20:46 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_plist	*ft_plistnew(t_proc *p)
{
	t_plist	*list;

	if ((list = (t_plist*)ft_memalloc(sizeof(t_plist))) == NULL)
		return (NULL);
	list->p = p;
	list->prev = NULL;
	list->next = NULL;
	return (list);
}

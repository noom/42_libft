/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistadd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:47:26 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/18 17:17:15 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_plist	*ft_plistadd(t_plist **list, t_proc *p)
{
	t_plist	*new;

	if ((new = ft_plistnew(p)) == NULL)
		return (NULL);
	new->prev = NULL;
	new->next = *list;
	*list = new;
	return (*list);
}

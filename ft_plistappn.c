/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistappn.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:37:48 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/18 17:19:07 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_plist	*ft_plistappn(t_plist **list, t_proc *p)
{
	if (*list != NULL)
		return (ft_plistapp(*list, p));
	if ((*list = ft_plistnew(p)) == NULL)
		return (NULL);
	return (*list);
}

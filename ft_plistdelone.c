/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistdelone.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:50:28 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/30 16:47:17 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_plistdelone(t_plist **list)
{
	t_plist	*prev;
	t_plist	*next;

	prev = (*list)->prev;
	next = (*list)->next;
	free((*list)->p);
	free(*list);
	*list = next;
	if (*list != NULL)
		(*list)->prev = prev;
	if (prev != NULL)
		prev->next = *list;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistapp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:42:07 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/18 17:18:41 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_plist	*ft_plistapp(t_plist *list, t_proc *p)
{
	t_plist	*new;
	t_plist	*last;

	if ((new = ft_plistnew(p)) == NULL)
		return (NULL);
	last = ft_plistlast(list);
	new->prev = last;
	last->next = new;
	return (new);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 12:34:12 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/30 16:57:52 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_bst	*ft_bstnew(void *data)
{
	t_bst *tree;

	if ((tree = (t_bst*)malloc(sizeof(t_bst))) == NULL)
		return (NULL);
	tree->data = data;
	tree->right = NULL;
	tree->left = NULL;
	return (tree);
}

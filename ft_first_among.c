/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_first_among.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/31 18:54:01 by noom              #+#    #+#             */
/*   Updated: 2018/07/31 18:54:28 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_first_among(char *s, char *chars, int start)
{
	int		i;

	start++;
	while (s[start] != '\0')
	{
		i = -1;
		while (chars[++i])
		{
			if (chars[i] == s[start])
				return (start - 1);
		}
		start++;
	}
	return (start);
}

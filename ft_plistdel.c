/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistdel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:50:28 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/30 16:47:05 by noom             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_plistdel(t_plist **list)
{
	t_plist	*iter;
	t_plist	*tmp;

	iter = *list;
	while (iter != NULL)
	{
		tmp = iter->next;
		free(iter->p);
		free(iter);
		iter = tmp;
	}
	*list = NULL;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_r.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 21:39:20 by echojnow          #+#    #+#             */
/*   Updated: 2018/07/17 16:59:24 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_r_free(t_slist **ps)
{
	t_slist	*iter;
	t_slist	*tmp;

	iter = *ps;
	while (iter)
	{
		tmp = iter->next;
		free(iter->data);
		free(iter);
		iter = tmp;
	}
	*ps = NULL;
}

static void	ft_r_store(int *p)
{
	static t_slist	*ps = NULL;

	if (p)
		ft_slistaddn(&ps, p);
	else
		ft_r_free(&ps);
}

int			*ft_r(int len, ...)
{
	va_list	va;
	int		*new;
	int		i;

	if (len == 0)
	{
		ft_r_store(NULL);
		return (NULL);
	}
	va_start(va, len);
	new = (int*)malloc(sizeof(int) * len);
	ft_r_store(new);
	i = -1;
	while (++i < len)
		new[i] = va_arg(va, int);
	va_end(va);
	return (new);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_plistaddn.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 14:48:27 by echojnow          #+#    #+#             */
/*   Updated: 2018/06/18 17:17:49 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_plist	*ft_plistaddn(t_plist **list, t_proc *p)
{
	if (*list != NULL)
		return (ft_plistadd(list, p));
	if ((*list = ft_plistnew(p)) == NULL)
		return (NULL);
	return (*list);
}

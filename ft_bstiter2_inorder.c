/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bstiter2_inorder.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: echojnow <echojnow@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 12:42:48 by echojnow          #+#    #+#             */
/*   Updated: 2018/01/16 16:16:14 by echojnow         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bstiter2_inorder(t_bst *tree, void (*f)(t_bst *data))
{
	if (tree == NULL)
		return ;
	ft_bstiter2_inorder(tree->left, f);
	f(tree);
	ft_bstiter2_inorder(tree->right, f);
}
